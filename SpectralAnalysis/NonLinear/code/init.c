#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double power(double, double);

void initialize(int N, double dx, int G, double *x, double *u)
{
	const double PI = 4 * atan(1.0);

	for (int i = 0; i < N+2*G; i++){
		x[i] = (i-G) * dx;
		u[i] = 0;
	}

	for (int k = 1; k <= N/2; k++){
		double phi = 2*PI * ((double)rand())/((double)RAND_MAX) - PI;
		for (int i = 0; i < N+2*G; i++){
			u[i] += cos(k*x[i] + phi) * power((double)k, -5.0/6.0);
		}
	}

	FILE *out;
	out = fopen("init.dat","w");
	for (int i = G; i < N+G+1; i++) 
		fprintf(out,"%1.16e %1.16e %1.16e %1.16e\n",x[i],u[i],(u[i+1]-u[i-1])/(2.0*dx),(u[i+1]-2*u[i]+u[i-1])/(dx*dx));
	fclose(out);
}



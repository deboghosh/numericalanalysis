#include <stdio.h>
#include <stdlib.h>

void periodic_tridiag(int N, double *a, double *b, double *c, double *rhs){
	int i;
	for (i = 0; i < N-3; i++){
		if (b[i] == 0){
			printf("ERROR: Zero encountered on main diagonal.\n");
			return;
		}
		double factor = a[i+1] / b[i];
		b[i+1] -= factor * c[i];
		a[i+1] = -factor * a[i];
		rhs[i+1] -= factor * rhs[i];
	}
	if (b[N-3] == 0){
		printf("ERROR: Zero encountered on main diagonal.\n");
		return;
	}
	double factor0 = a[N-2] / b[N-3];
	b[N-2] -= factor0 * c[N-3];
	c[N-2] -= factor0 * a[N-3];
	rhs[N-2] -= factor0 * rhs[N-3];

	for (i = 0; i < N-3; i++){
		double factor = c[N-1] / b[i];
		c[N-1] = - factor * c[i];
		b[N-1] -= factor * a[i];
		rhs[N-1] -= factor * rhs[i];
	}

	double factor1 = c[N-1] / b[N-3];
	a[N-1] -= factor1 * c[N-3];
	b[N-1] -= factor1 * a[N-3];
	rhs[N-1] -= factor1 * rhs[N-3];

	double factor2 = a[N-1] / b[N-2];
	b[N-1] -= factor2 * c[N-2];
	rhs[N-1] -= factor2 * rhs[N-2];

	rhs[N-1] /= b[N-1];
	rhs[N-2] = (rhs[N-2] - c[N-2] * rhs[N-1]) / b[N-2];
	for (i = N-3; i >= 0; i--){
		rhs[i] = (rhs[i] - a[i]*rhs[N-1] - c[i]*rhs[i+1]) / b[i];
	}
}

void LU_decomp(int N, double **A, double *rhs){
	if ((!A) || (!rhs)){
		return;
	}else{
		int i, j, k;
		for (i = 0; i < N; i++){
			if (A[i][i] == 0){
				printf("ERROR: Zero encountered on main diagonal.\n");
				return;
			}
			for (j = i+1; j < N; j++){
				double factor = A[j][i] / A[i][i];
				A[j][i] = 0;
				for (k = i+1; k < N; k++){
					A[j][k] -= factor * A[i][k];
				}
				rhs[j] -= factor * rhs[i];
			}
		}
		for (i = N-1; i >=0; i--){
			double sum = 0;
			for (j = i+1; j < N; j++){
				sum += A[i][j] * rhs[j];
			}
			rhs[i] = (rhs[i] - sum) / A[i][i];
		}
	}
	return;
}

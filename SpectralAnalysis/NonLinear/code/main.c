#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int N_MAPPINGS, BORGES, CARPENTER;
double IS_p;

const double  PI = 4 * atan(1.0);

void initialize       (int, double, int, double*, double*);
void reconstruct      (int,int,double*,double*,double*,int,int);
void fourier_analysis (int,int,double*,double*,double*,double*);

int main()
{
	double *x, *u, *ux, *kr, *ki;
	int space_order, ghost;
	int space_scheme;
  FILE *out;

	space_order = 5;
	space_scheme = -2;
	ghost = 5;

	N_MAPPINGS = 0;
	BORGES = 0;
	CARPENTER = 0;
	IS_p = 2;

  int Nr = 50000;

	int N = 200;
	double L = 2.0*PI;

	double dx = L / ((double) (N));

  double *mur,*mui, *sdr, *sdi;
  mur = (double*) calloc (N,sizeof(double));
  mui = (double*) calloc (N,sizeof(double));
  sdr = (double*) calloc (N,sizeof(double));
  sdi = (double*) calloc (N,sizeof(double));
	srand (time(NULL));

  int n;
  double **wvr, **wvi;
  wvr = (double**) calloc (Nr, sizeof(double*));
  wvi = (double**) calloc (Nr, sizeof(double*));
  for (n = 0; n < Nr; n++) {
    wvr[n] = (double*) calloc (N,sizeof(double));
    wvi[n] = (double*) calloc (N,sizeof(double));
  }

  out = fopen("Eigenvalues.dat","w");
  for (n = 0; n < Nr; n++) {
	  x  = (double*) calloc(N+2*ghost, sizeof(double));
	  u  = (double*) calloc(N+2*ghost, sizeof(double));
	  ux = (double*) calloc(N+2*ghost, sizeof(double));
	  kr = (double*) calloc(N, sizeof(double));
	  ki = (double*) calloc(N, sizeof(double));
	  if ((n+1)%10000 == 0) printf("%7dd\tSetting initial solution...\n",n+1);
	  initialize(N, dx, ghost, x, u);
    if ((n+1)%10000 == 0) printf("%7d\tCalculating derivative...\n",n+1);
    reconstruct(N,ghost,ux,u,x,space_order,space_scheme);
    if ((n+1)%10000 == 0) printf("%7d\tPerforming spectral analysis...\n",n+1);
    fourier_analysis(N,ghost,u,ux,kr,ki);

    for (int i = 0; i < N; i++) {
      wvr[n][i] = (kr[i]/(N/2)*PI);
      wvi[n][i] = (ki[i]/(N/2)*PI);
      fprintf(out,"%1.16E  %1.16E\n",wvi[n][i],wvr[n][i]);
    }
	  free(x);
	  free(u);
	  free(ux);
    free(kr);
    free(ki);
  }

  /* calculating average */
  printf("Calculating average...\n");
  for (int i = 0; i < N; i++) {
    mur[i] = 0;
    mui[i] = 0;
    for (int n = 0; n < Nr; n++) {
      mur[i] += wvr[n][i];
      mui[i] += wvi[n][i];
    }
    mur[i] /= Nr;
    mui[i] /= Nr;

    if (i == N/2) mui[i] = 0;
  }
  printf("Calculating standard deviation...\n");
  /* calculating standard deviation */
  for (int i = 0; i < N; i++) {
    sdr[i] = 0;
    sdi[i] = 0;
    for (int n = 0; n < Nr; n++) {
      sdr[i] += (wvr[n][i]-mur[i]) * (wvr[n][i]-mur[i]);
      sdi[i] += (wvi[n][i]-mui[i]) * (wvi[n][i]-mui[i]);
    }
    sdr[i] = sqrt(sdr[i]/(Nr-1));
    sdi[i] = sqrt(sdi[i]/(Nr-1));
    if (i == N/2) sdi[i] = 0;
  }

  out = fopen("wavenumber.dat","w");
  for (int i = 0; i < N; i++)
    fprintf(out,"%3d %1.16E %1.16E %1.16E %1.16E\n",i,mur[i],mui[i],sdr[i],sdi[i]);
  fclose(out);

  for (n = 0; n < Nr; n++) {
    free(wvr[n]);
    free(wvi[n]);
  }
  free(wvr);
  free(wvi);
  free(mur);
  free(mui);
  free(sdr);
  free(sdi);
  return(0);
}

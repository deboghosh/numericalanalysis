#include <math.h>

double absolute(double x)
{
  return (x<0 ? -x : x);
}

double power(double x, double a) 
{
	return exp(a*log(x));
}

double max(double a, double b)
{
  return (a>b ? a : b);
}

double max3(double a, double b, double c)
{
	return max(max(a,b), max(a,c));
}

double min(double a, double b)
{
  return(a<b ? a : b);
}

double min3(double a, double b, double c)
{
	return min(min(a,b), min(a,c));
}

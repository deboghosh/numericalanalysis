#include <math.h>
#include <fftw3.h>

double abs(double);

void fourier_analysis(int N, int G, double *u, double *ux, double *real, double *imag){
	int i;
	fftw_complex *in_u, *out_u;
	fftw_complex *in_ux, *out_ux;
	fftw_plan pu, pux;

	in_u = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (N));
	out_u = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (N));
	pu = fftw_plan_dft_1d(N, in_u, out_u, FFTW_FORWARD, FFTW_ESTIMATE);
	in_ux = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (N));
	out_ux = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (N));
	pux = fftw_plan_dft_1d(N, in_ux, out_ux, FFTW_FORWARD, FFTW_ESTIMATE);

	for (i = 0; i < N; i++){
		in_u[i][0] = u[i+G];
		in_u[i][1] = 0;
		in_ux[i][0] = ux[i+G];
		in_ux[i][1] = 0;
	}

	fftw_execute(pu);
	fftw_execute(pux);
	for (i = 0; i < N; i++){
		out_u[i][0] /= (N);
		out_u[i][1] /= (N);
		out_ux[i][0] /= (N);
		out_ux[i][1] /= (N);
	}

  /* calculate modified wavenumber */
	for (i = 0; i < N; i++){
    double a = out_ux[i][0];
    double b = out_ux[i][1];
    double c = out_u[i][0];
    double d = out_u[i][1];
    double denom = - (c*c + d*d);

    real[i] = (a*d-b*c) / denom;
    imag[i] = (b*d+a*c) / denom;
	}

	fftw_destroy_plan(pu);
	fftw_free(in_u);
	fftw_free(out_u);
	fftw_destroy_plan(pux);
	fftw_free(in_ux);
	fftw_free(out_ux);
}

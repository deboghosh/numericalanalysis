#include <stdio.h>
#include <math.h>

int main(){
	int     N = 400, i;
	double  pi = 4*atan(1.0);
	FILE    *out;

	printf("Second order central (non-compact)\n");
	out=fopen("second_order_central_noncompact.dat","w");
	for (i = 0; i < N; i++){
		double a, b, c, aa, bb;
		aa = 0.0;
		bb = 0.0;
		a = 1.0;
		b = 0.0;
		c = 0.0;
		double t = (double) i * (2*pi/((double) (N-1)));
		double mod_phase = (a*sin(t)+(b/2.0)*sin(2*t)+(c/3.0)*sin(3*t))/ (1+2*aa*cos(t)+2*bb*cos(2*t));
		fprintf(out,"%+1.16e %+1.16e\n",t,mod_phase);
	}
	fclose(out);

	printf("Fourth order central\n");
	out=fopen("fourth_order_central_noncompact.dat","w");
	for (i = 0; i < N; i++){
		double a, b, c, aa, bb;
		aa = 0.0;
		bb = 0.0;
		a = 4.0/3.0;
		b = -1.0/3.0;
		c = 0.0;
		double t = (double) i * (2*pi/((double) (N-1)));
		double mod_phase = (a*sin(t)+(b/2.0)*sin(2*t)+(c/3.0)*sin(3*t))/ (1+2*aa*cos(t)+2*bb*cos(2*t));
		fprintf(out,"%+1.16e %+1.16e\n",t,mod_phase);
	}
	fclose(out);

	printf("Fourth order tri-diagonal\n");
	out=fopen("fourth_order_tridiagonal.dat","w");
	for (i = 0; i < N; i++){
		double a, b, c, aa, bb;
		aa = 0.25;
		bb = 0.0;
		a = 1.5;
		b = 0.0;
		c = 0.0;
		double t = (double) i * (2*pi/((double) (N-1)));
		double mod_phase = (a*sin(t)+(b/2.0)*sin(2*t)+(c/3.0)*sin(3*t))/ (1+2*aa*cos(t)+2*bb*cos(2*t));
		fprintf(out,"%+1.16e %+1.16e\n",t,mod_phase);
	}
	fclose(out);

	printf("Sixth order central (non-compact)\n");
	out=fopen("sixth_order_central_noncompact.dat","w");
	for (i = 0; i < N; i++){
		double a, b, c, aa, bb;
		aa = 0.0;
		bb = 0.0;
		a = (1.0/6.0) * (aa + 9.0);
		b = (1.0/15.0) * (32.0*aa - 9.0);
		c = (1.0/10.0) * (-3.0*aa + 1.0);
		double t = (double) i * (2*pi/((double) (N-1)));
		double mod_phase = (a*sin(t)+(b/2.0)*sin(2*t)+(c/3.0)*sin(3*t))/ (1+2*aa*cos(t)+2*bb*cos(2*t));
		fprintf(out,"%+1.16e %+1.16e\n",t,mod_phase);
	}
	fclose(out);

	printf("Sixth order tri-diagonal\n");
	out=fopen("sixth_order_tridiagonal.dat","w");
	for (i = 0; i < N; i++){
		double a, b, c, aa, bb;
		aa = 1.0/3.0;
		bb = 0.0;
		a = (1.0/6.0) * (aa + 9.0);
		b = (1.0/15.0) * (32.0*aa - 9.0);
		c = (1.0/10.0) * (-3.0*aa + 1.0);
		double t = (double) i * (2*pi/((double) (N-1)));
		double mod_phase = (a*sin(t)+(b/2.0)*sin(2*t)+(c/3.0)*sin(3*t))/ (1+2*aa*cos(t)+2*bb*cos(2*t));
		fprintf(out,"%+1.16e %+1.16e\n",t,mod_phase);
	}
	fclose(out);

	printf("Sixth order penta-diagonal\n");
	out=fopen("sixth_order_pentadiagonal.dat","w");
	for (i = 0; i < N; i++){
		double a, b, c, aa, bb;
		aa = 1.0/2.0;
		bb = (1.0/12.0) * (-1.0 + 3.0*aa);
		a = (2.0/9.0) * (8.0 - 3.0*aa);
		b = (1.0/18.0) * (-17.0 + 57.0*aa);
		c = 0.0;
		double t = (double) i * (2*pi/((double) (N-1)));
		double mod_phase = (a*sin(t)+(b/2.0)*sin(2*t)+(c/3.0)*sin(3*t))/ (1+2*aa*cos(t)+2*bb*cos(2*t));
		fprintf(out,"%+1.16e %+1.16e\n",t,mod_phase);
	}
	fclose(out);
}

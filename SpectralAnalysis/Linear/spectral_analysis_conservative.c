#include <stdio.h>
#include <math.h>

int main(){
	int     N = 400, i;
	double  pi = 4*atan(1.0);
	FILE    *out;

	printf("First order upwind (non-compact)\n");
	out = fopen("first_order_upwind_noncompact.dat","w");
	for (i = 0; i < N; i++){
		double a, b, c, d, e, g, aa, bb, cc;
    
    /* Right hand side */
		a = 0.0;		/* j-2   */
		b = 0.0;		/* j-1   */
		c = 1.0;		/* j     */
		d = 0.0;		/* j+1   */
		e = 0.0;		/* j+2   */
		g = 0.0;		/* j+3   */
    /* Left hand side	*/
		aa = 0.0;		/* j-1/2 */
		bb = 1.0;		/* j+1/2 */
		cc = 0.0;		/* j+3/2 */

		double t = (double) i * (2*pi/((double) (N-1)));
		double A, B, C, D;
		A = (aa-cc) * sin(t);
		B = bb + (aa+cc) * cos(t);
		C = (g-a)*cos(3*t) + (e+a-b-g)*cos(2*t) + (b+d-c-e)*cos(t) + (c-d);
		D = (g+a)*sin(3*t) + (e-a+b-g)*sin(2*t) + (d+c-e-b)*sin(t);

		double phase, dissipation;
		phase =  (A*C + B*D) / (A*A + B*B);
		dissipation =  (A*D - B*C) / (A*A + B*B);

		fprintf(out,"%+1.16e %+1.16e %+1.16e\n",t,phase,dissipation);
	}
	fclose(out);

	printf("Fifth order upwind (non-compact)\n");
	out=fopen("fifth_order_upwind_noncompact.dat","w");
	for (i = 0; i < N; i++){
		double a, b, c, d, e, g, aa, bb, cc;

    /* Right-hand side */
		a = 1.0/30.0;		/* j-2   */
		b = -13.0/60.0;		/* j-1   */
		c = 47.0/60.0;		/* j     */
		d = 27.0/60.0;		/* j+1   */
		e = -1.0/20.0;		/* j+2   */
		g = 0.0;		/* j+3   */
    /* Left hand side */	
		aa = 0.0;		/* j-1/2 */
		bb = 1.0;		/* j+1/2 */
		cc = 0.0;		/* j+3/2 */

		double t = (double) i * (2*pi/((double) (N-1)));
		double A, B, C, D;
		A = (aa-cc) * sin(t);
		B = bb + (aa+cc) * cos(t);
		C = (g-a)*cos(3*t) + (e+a-b-g)*cos(2*t) + (b+d-c-e)*cos(t) + (c-d);
		D = (g+a)*sin(3*t) + (e-a+b-g)*sin(2*t) + (d+c-e-b)*sin(t);

		double phase, dissipation;
		phase =  (A*C + B*D) / (A*A + B*B);
		dissipation =  (A*D - B*C) / (A*A + B*B);

		fprintf(out,"%+1.16e %+1.16e %+1.16e\n",t,phase,dissipation);
	}
	fclose(out);

	printf("Fifth order upwind (compact)\n");
	out=fopen("fifth_order_upwind_compact.dat","w");
	for (i = 0; i < N; i++){
		double a, b, c, d, e, g, aa, bb, cc;

    /* Right-hand side */
		a = 0.0;		/* j-2   */
		b = 1.0/30.0;		/* j-1   */
		c = 19.0/30.0;		/* j     */
		d = 1.0/3.0;		/* j+1   */
		e = 0;			/* j+2   */
		g = 0.0;		/* j+3   */
    /* Left hand side */	
		aa = 0.3;		/* j-1/2 */
		bb = 0.6;		/* j+1/2 */
		cc = 0.1;		/* j+3/2 */

		double t = (double) i * (2*pi/((double) (N-1)));
		double A, B, C, D;
		A = (aa-cc) * sin(t);
		B = bb + (aa+cc) * cos(t);
		C = (g-a)*cos(3*t) + (e+a-b-g)*cos(2*t) + (b+d-c-e)*cos(t) + (c-d);
		D = (g+a)*sin(3*t) + (e-a+b-g)*sin(2*t) + (d+c-e-b)*sin(t);

		double phase, dissipation;
		phase =  (A*C + B*D) / (A*A + B*B);
		dissipation =  (A*D - B*C) / (A*A + B*B);

		fprintf(out,"%+1.16e %+1.16e %+1.16e\n",t,phase,dissipation);
	}
	fclose(out);

	printf("Fifth order upwind low-dissipation (compact)\n");
	out=fopen("fifth_order_upwind_compactLD.dat","w");
	for (i = 0; i < N; i++){
		double a, b, c, d, e, g, aa, bb, cc;

    /* Right-hand side */
		a = 0.0;		/* j-2   */
		b = 3.0/120.0;		/* j-1   */
		c = 67.0/120.0;		/* j     */
		d = 49.0/120.0;		/* j+1   */
		e = 1.0/120.0;		/* j+2   */
		g = 0.0;		/* j+3   */
    /* Left hand side */	
		aa = 5.0/20.0;		/* j-1/2 */
		bb = 12.0/20.0;		/* j+1/2 */
		cc = 3.0/20.0;		/* j+3/2 */

		double t = (double) i * (2*pi/((double) (N-1)));
		double A, B, C, D;
		A = (aa-cc) * sin(t);
		B = bb + (aa+cc) * cos(t);
		C = (g-a)*cos(3*t) + (e+a-b-g)*cos(2*t) + (b+d-c-e)*cos(t) + (c-d);
		D = (g+a)*sin(3*t) + (e-a+b-g)*sin(2*t) + (d+c-e-b)*sin(t);

		double phase, dissipation;
		phase =  (A*C + B*D) / (A*A + B*B);
		dissipation =  (A*D - B*C) / (A*A + B*B);

		fprintf(out,"%+1.16e %+1.16e %+1.16e\n",t,phase,dissipation);
	}
	fclose(out);

	printf("Ninth order upwind (non-compact)\n");
	out=fopen("ninth_order_upwind_noncompact.dat","w");
	for (i = 0; i < N; i++){
		double t = (double) i * (2*pi/((double) (N-1)));
		double phase, dissipation;
		dissipation = - ((1-cos(t))*(1879+734*cos(t)-106*cos(2*t)+14*cos(3*t)-1*cos(4*t))/2520
			-sin(t)*(2016*sin(t)-504*sin(2*t)+96*sin(3*t)-9*sin(4*t))/2520);
		phase = sin(t)*(1879+734*cos(t)-106*cos(2*t)+14*cos(3*t)-1*cos(4*t))/2520
			+(1-cos(t))*(2016*sin(t)-504*sin(2*t)+96*sin(3*t)-9*sin(4*t))/2520;

		fprintf(out,"%+1.16e %+1.16e %+1.16e\n",t,phase,dissipation);;
	}
	fclose(out);

  return(0);

}

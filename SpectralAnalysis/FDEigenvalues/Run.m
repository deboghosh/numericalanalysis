%% Initialize
clc;
clear all;
close all;
ifig = 1;

% Grid size
N = 100;
% Boundary type
boundary = 'periodic';

%% Initialize grid
if (strcmp(strtrim(boundary),'periodic'))
    dx = 1.0/N;
else
    dx = 1.0/(N-1);
end
x = 0:dx:(N-1)*dx;

%% plotting styles
style = {'bo';'ko';'go';'ks';'ro'};
schemes = {'uw1';'uw3';'uw5';'cuw5';'2c'};

%% Construct the discretization matrix and compute spectrum
for i=1:size(schemes,1)
    fprintf('Computing discretization matrix.\n');
    InterpMatrix = GetInterpOperator(N,schemes(i,:),boundary);
    FDMatrix = GetFDOperator(N);
    DiscretMatrix = -FDMatrix*InterpMatrix/dx;
    fprintf('Computing spectrum.\n');
    lambda = eig(DiscretMatrix);
    % plot
    figure(ifig);
    plot(real(lambda),imag(lambda),char(style(i)));
    hold on;
    figure(ifig+1);
    plot(real(lambda),char(style(i)));
    hold on;
    figure(ifig+2);
    plot(imag(lambda),char(style(i)));
    hold on;
end


%% done
figure(ifig);
title('Eigenvalues of discretization operators');
axis equal;
grid on;
legend(schemes);
hold off;

%% done
figure(ifig+1);
title('Dissipation');
grid on;
legend(schemes);
hold off;

%% done
figure(ifig+2);
title('Dispersion');
grid on;
legend(schemes);
hold off;

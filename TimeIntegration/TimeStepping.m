addpath(genpath('./chebfun'));

C = 'color'; c = {'k','b','r','g','k','k'};
x = [0 0]; y = [-8 8]; K = 'k'; LW = 'linewidth'; FS = 'fontsize';

close all;
scrsz = get(0,'ScreenSize');
width = scrsz(4);
height =0.8*width;
figPlot = figure('Position',[1 scrsz(4)/2 width height]);

t = chebfun('t',[0 2*pi]);
%% First order
z = exp(1i*t);
w = z-1;
figure(figPlot);
plot(w,C,c{1},LW,2);
hold on;
%% Second order
for i = 1:3
  w = w-(1+w+.5*w.^2-z.^2)./(1+w);
end
figure(figPlot);
plot(w,'-',C,c{2},LW,2);
hold on;
%% Third order
for i = 1:4
  w = w-(1+w+.5*w.^2+w.^3/6-z.^3)./(1+w+w.^2/2);
end
figure(figPlot);
plot(w,'-',C,c{3},LW,2);
hold on;
%% Fourth order
for i = 1:4
  w = w-(1+w+.5*w.^2+w.^3/6+w.^4/24-z.^4)...
      ./(1+w+w.^2/2+w.^3/6);
end
figure(figPlot);
plot(w,'-',C,c{4},LW,2);
axis([-3 0.5 -3.5 3.5]);
axis square;
grid on;
hold off;
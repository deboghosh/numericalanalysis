clc;
clear all;
close all;
ifig = 1;

%% Set some parameters 

% Grid size
N = 100;

% Physical model('linadv1d','euler1d')
model = 'euler1d';

% Boundary type
boundary = 'periodic';

% Discretization method ('1','weno5','crweno5')
sp_method = 'weno5';

% shift
shift = 1.0;

% time step size
dt = 0.01;

%% Flow variables
gamma = 1.4;
rho_inf = 1.0;
drho = 0.1;
u_inf = 0.1;
p_inf = rho_inf/gamma;

%% Initialize grid
if (strcmp(strtrim(boundary),'periodic'))
    dx = 1.0/N;
else
    dx = 1.0/(N-1);
end
x = 0:dx:(N-1)*dx;

%% Initialize solution
if (strcmp(model,'linadv1d'))
    nvar = 1;
    U = sin(x);
    GetFluxJacobian = @GetFluxJacobian_LinAdv1D;
elseif (strcmp(model,'euler1d'))
    nvar = 3;
    rho = rho_inf+drho*sin(x);
    u = u_inf*ones(1,size(x,2));
    p = p_inf*ones(1,size(x,2));
    e = p/(gamma-1) + 0.5 * rho .* u.^2;
    U = [rho;rho.*u;e];
    GetFluxJacobian = @GetFluxJacobian_Euler1D;
else
    fprintf('Error: unknown physical model %s\n',model);
    return;
end
ndof = N * nvar;

%% Construct the discretization matrix
fprintf('Computing discretization matrix.\n');
InterpMatrix = GetInterpOperator(N,sp_method,boundary);
FDMatrix = GetFDOperator(N);
DiscretMatrix = -FDMatrix*InterpMatrix/dx;

%% Compute and plot the spectrum of the discretization matrix
figure(ifig);
fprintf('Computing spectrum.\n');
lambda = eig(DiscretMatrix);
figure(ifig);
plot(real(lambda),imag(lambda),'bo');
title('Eigenvalues of the discretization operator');
axis equal;
grid on;
ifig = ifig + 1;

%% Construct the RHS Jacobian matrix
fprintf('Computing Jacobian.\n');
RHSJac = zeros(ndof,ndof);
for i = 1:N
    for j = 1:N
        Amat = feval(GetFluxJacobian,U(:,j),gamma);
        for k = 1:nvar
            for l = 1:nvar
                RHSJac(nvar*(i-1)+k,nvar*(j-1)+l) = DiscretMatrix(i,j) * Amat(k,l);
            end
        end
    end
end

%% Compute and plot the spectrum of the RHS Jacobian matrix
figure(ifig);
fprintf('Computing spectrum.\n');
lambda = eig(RHSJac);
figure(ifig);
plot(real(lambda),imag(lambda),'bo');
title('Eigenvalues of the RHS Jacobian');
axis equal;
grid on;
ifig = ifig + 1;

%% Construct the Jacobian for implicit time integration
fprintf('Computing time integration Jacobian.\n');
TSJac = 1.0/(shift*dt)*eye(ndof,ndof) - RHSJac;

%% Compute and plot the spectrum of the time integration Jacobian matrix
figure(ifig);
fprintf('Computing spectrum.\n');
lambda = eig(TSJac);
figure(ifig);
plot(real(lambda),imag(lambda),'bo');
title('Eigenvalues of the implicit time-integration Jacobian');
axis equal;
grid on;
ifig = ifig + 1;
fprintf('Condition number (implicit time integration Jacobian): %1.16e\n',cond(TSJac));

%% Construct a preconditioning matrix
fprintf('Constructing the preconditioning matrix.\n');
% PreconMatrix = eye(ndof);
% PreconMatrix = inv(TSJac); %#ok<MINV>
% PreconMatrix = zeros(ndof,ndof);
% for i = 1:N
%     j = i;
%     for k = 1:nvar
%         for l = 1:nvar
%             PreconMatrix((i-1)*nvar+k,(j-1)*nvar+l) = TSJac((i-1)*nvar+k,(j-1)*nvar+l);
%         end
%     end
%     if (i > 1)
%         j = i-1;
%         for k = 1:nvar
%             for l = 1:nvar
%                 PreconMatrix((i-1)*nvar+k,(j-1)*nvar+l) = TSJac((i-1)*nvar+k,(j-1)*nvar+l);
%             end
%         end
%     end
%     if (i < N)
%         j = i+1;
%         for k = 1:nvar
%             for l = 1:nvar
%                 PreconMatrix((i-1)*nvar+k,(j-1)*nvar+l) = TSJac((i-1)*nvar+k,(j-1)*nvar+l);
%             end
%         end
%     end
% end
InterpMatrix_PC = GetInterpOperator(N,'1',boundary);
DiscretMatrix_PC = -FDMatrix*InterpMatrix_PC/dx;
RHSJac_PC = zeros(ndof,ndof);
for i = 1:N
    for j = 1:N
        Amat = feval(GetFluxJacobian,U(:,j),gamma);
        for k = 1:nvar
            for l = 1:nvar
                RHSJac_PC(nvar*(i-1)+k,nvar*(j-1)+l) = DiscretMatrix_PC(i,j) * Amat(k,l);
            end
        end
    end
end
PreconMatrix = 1.0/(shift*dt)*eye(ndof,ndof) - RHSJac_PC;
PreconMatrixInv = inv(PreconMatrix);

%% Compute and plot the spectrum of the preconditioned time integration Jacobian matrix
PreconTSJac = PreconMatrixInv * TSJac;
figure(ifig);
fprintf('Computing spectrum.\n');
lambda = eig(PreconTSJac);
figure(ifig);
plot(real(lambda),imag(lambda),'bo');
title('Eigenvalues of the preconditioned implicit time-integration Jacobian');
axis equal;
grid on;
ifig = ifig + 1;
fprintf('Condition number (preconditioned system): %1.16e\n',cond(PreconTSJac));
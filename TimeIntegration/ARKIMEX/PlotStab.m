clear all;
close all;

L_evals_file=input('Enter IFunction eigenvalues file: ','s');
SL_evals_file=input('Enter RHSFunction eigenvalues file: ','s');
dt=input('Enter time step size: ');
Nevals = input('Enter number of eigenvalues to consider: ');

L_evals=load(L_evals_file);
TotalEvals = size(L_evals,1);
L_evals=L_evals([1:int32(Nevals/2),TotalEvals-int32(Nevals/2)+1:TotalEvals],2:3)*dt;
SL_evals=load(SL_evals_file);
SL_evals_full=SL_evals(:,2:3)*dt;
SL_evals=SL_evals([1:int32(Nevals/2),TotalEvals-int32(Nevals/2)+1:TotalEvals],2:3)*dt;

METHODS_TO_SHOW=2;
METHOD_1 = 'ARK2e';IMEX_FLAG(1)=1;
METHOD_2 = 'ARK2e';IMEX_FLAG(2)=0;

METHOD_IMSELECT=1;

XRes=50;
YRes=2*XRes;

figure; set(gca,'FontSize',12);hold on;

X_EI=L_evals(:,1);
Y_EI=L_evals(:,2);

X_D=[-5,0.1];
Y_D=[-3,3];
X_E=linspace(X_D(1),X_D(2),XRes);
Y_E=linspace(Y_D(1),Y_D(2),YRes);
[X,Y] = meshgrid(X_E,Y_E);
plot(SL_evals_full(:,1),SL_evals_full(:,2),'b.','MarkerSize',12);
%plot(L_evals(:,1),abs(L_evals(:,2)),'r*','MarkerSize',14);
for i=1:METHODS_TO_SHOW
    eval(['METHOD=METHOD_' sprintf('%d',i) ';']);
    disp(METHOD);
    StabEx=zeros(size(X));
    if(IMEX_FLAG(i)==0)
        StabEx=Rstab(X,Y,0,0,METHOD);
    elseif(IMEX_FLAG(i)==1)
        for ii=1:length(X_EI)
            disp(['Step ' num2str(ii) ' out of ' num2str(length(X_EI))]);
            for jj=1:length(Y_EI)
                StabEx=max(Rstab(X,Y,X_EI(ii),Y_EI(jj),METHOD),StabEx);
            end;
        end;
    end;
    [c,h]=contour(X,Y,StabEx,[1,1]);
    hh(i)=h;
    StabExI(i,:,:)=StabEx;
    switch(i)
        case 1
            set(h,'LineWidth',1);
            set(h,'color','k');
            set(h,'LineStyle','-');
        case 2
            set(h,'LineWidth',1);
            set(h,'color','k');
            set(h,'LineStyle',':');
        otherwise
            error('error');
    end;
end;
legend(hh,[METHOD_1,' (IMEX)'],[METHOD_2, ' (Expl)  '],'Location','northwest');

XLIM=get(gca,'XLim');
YLIM=get(gca,'YLim');
plot(XLIM,[0 0],'k-','LineWidth',1);
plot([0 0],YLIM,'k-','LineWidth',1);
xlabel('Re(\lambda) \Delta t','FontSize',14);
ylabel('Im(\lambda) \Delta t','FontSize',14);
box off;
grid on;
print(1,'-depsc2','figure.eps');
saveas(1,'figure.fig');
return;
